---
layout: page
title: "Linux App Summit"
permalink: /
---

# Introduction

The Linux App Summit (LAS) brings together the developers that grow the Linux application ecosystem. Fostering collaboration between app and system developers, LAS aims to help streamline and improve the integration between user-facing programs and the operating system.

At LAS you can attend talks, panels and Q&As on a wide range of topics, covering everything from creating to packaging and distributing apps, all delivered by the top experts in each field. You will acquire insights into how to reach users and build a community, what toolkits and technologies make development easier, which platforms to aim for, and much more.

If you are an app developer or a kernel developer interested in user space, you will want to come to LAS.
